package curso.springboot.springboot;
//essa classe é a que ativa o spring
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.Ordered;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


//para funcionar tem q ativar recursos com as anotações
@SpringBootApplication
@EntityScan(basePackages="curso.springboot.model") //aqui é o nome do pacote onde vai conter as classes de perssitencia
@ComponentScan(basePackages= {"curso.*"}) //usar essea anotação para ele encontrar os pacotes de controle do projeto
@EnableJpaRepositories(basePackages= {"curso.springboot.repository"}) //informar o pacote de repository pacote do crud
@EnableTransactionManagement
@EnableWebMvc
//sem essa anotação ele não achava a classe de controle	
public class SpringbootApplication implements WebMvcConfigurer{

	public static void main(String[] args) {
		SpringApplication.run(SpringbootApplication.class, args);
		
		/*
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String result = encoder.encode("123");
		System.out.println(result); */
	}
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/login").setViewName("/login");
		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	}
}


//1ºpara configura visitou o site start.spring.io e colocou os recursos basicos, arq na pasta
//2ºcoloou as dependencias no pom
//3º config o arq application.properties
//4º criar o pacote model
//5ºinformar na classe springbotApplication o pacote a ser mapeado