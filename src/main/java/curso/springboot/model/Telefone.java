package curso.springboot.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

//como usa o spring não precisa colocar o nome da classe em um arquivo persistence
@Entity
public class Telefone {
	
	@Id //com spring não precisa colocar no arquivo persistence xml
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String numero;
	
	private String tipo;
	
	@ManyToOne //estou fazendo o relacionamento com a classe pessoa, muitos pra um
	private Pessoa pessoa;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	

}
