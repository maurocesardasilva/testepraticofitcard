package curso.springboot.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import curso.springboot.model.Pessoa;
import curso.springboot.model.Telefone;
import curso.springboot.repository.PessoaRepository;
import curso.springboot.repository.ProfissaoRepository;
import curso.springboot.repository.TelefonRepository;

@Controller // aanotação para reconhecer essa classe como controladora da tela
public class PessoaController {

	@Autowired
	private PessoaRepository pessoaRepository;

	@Autowired
	private TelefonRepository telefoneRepository;
	
	@Autowired
	private ReportUtil reportUtil; //classe de relatorio
	
	@Autowired
	private ProfissaoRepository profissaoRepository;
	
	
	// esse metodo responde ao receber requisição(get sempre q consulta, faz
	// redirecionamento
	// para salvar é post)
	// esse metodo vai encaminhar pra pagina cadastropessoa
	// (esse metodo direciona pra pag cadastro pessoa)
	/*
	 * @RequestMapping(method=RequestMethod.GET, value="/cadastropessoa") public
	 * String inicio() { return "cadastro/cadastropessoa";
	 * 
	 * }
	 */

	@RequestMapping(method = RequestMethod.GET, value = "/cadastropessoa")
	public ModelAndView inicio() {
		// ModelAndView indica que estou enviando infomações para tela
		ModelAndView andView = new ModelAndView("cadastro/cadastropessoa");
		// cadastro/cadastropessoa indica que vou ficar na mesma tela
		andView.addObject("pessoaobj", new Pessoa());

		Iterable<Pessoa> pessoasIt = pessoaRepository.findAll();
		andView.addObject("pessoas", pessoasIt);
		andView.addObject("pessoaobj", new Pessoa());
		andView.addObject("profissao", profissaoRepository.findAll()); //coloquei aqui pq quando abrir basicamente ele vai retornar para a tela preenchendo combo
		return andView;
	}

	// os ** ignora qualquer coisa antes do salvar pessoa
	@RequestMapping(method = RequestMethod.POST, value = "**/salvarpessoa")
	public ModelAndView salvar(@Valid Pessoa pessoa, BindingResult bindingResult) {
		//anotações valid e BindingResult são para valições
		
		pessoa.setTelefones(telefoneRepository.getTelefones(pessoa.getId())); //COMO COLOQUEI PARA OPERAR EM CASCATA NO MODEL PRECISO BUSCAR OS TELEFONES ANTES
			//inicio validações
		if(bindingResult.hasErrors()) {//haserros se tiver erro valida
			ModelAndView andView = new ModelAndView("cadastro/cadastropessoa");//retorna mesma tela
			Iterable<Pessoa> pessoasIt = pessoaRepository.findAll();
			andView.addObject("pessoas", pessoasIt);
			andView.addObject("pessoaobj", pessoa);//o pessoa é o obj que vem da tela, ele vai fazer retronar a validação
			
			List<String> msg = new ArrayList<String>();
			for(ObjectError objectError : bindingResult.getAllErrors()) {
				msg.add(objectError.getDefaultMessage()); //o getDefaultMessage vem das anotações
			}
			
			andView.addObject("msg", msg); 
			andView.addObject("profissao", profissaoRepository.findAll()); 
			return andView;
		}
		pessoaRepository.save(pessoa);

		ModelAndView andView = new ModelAndView("cadastro/cadastropessoa");
		Iterable<Pessoa> pessoasIt = pessoaRepository.findAll();
		andView.addObject("pessoas", pessoasIt);
		andView.addObject("pessoaobj", new Pessoa());
		andView.addObject("profissao", profissaoRepository.findAll()); 

		return andView;

	}

	// esse metodo é chamado quando na url for passado o param /listapessoa
	// sempre q for retornar pra tela tem que usar o obj modelandview

	@RequestMapping(method = RequestMethod.GET, value = "/listapessoas")
	public ModelAndView pessoas() {
		ModelAndView andView = new ModelAndView("cadastro/cadastropessoa");
		Iterable<Pessoa> pessoasIt = pessoaRepository.findAll();
		andView.addObject("pessoas", pessoasIt);
		andView.addObject("pessoaobj", new Pessoa());
		return andView;
	}

	@GetMapping("/editarpessoa/{idpessoa}")
	public ModelAndView editar(@PathVariable("idpessoa") Long idpessoa) {

		Optional<Pessoa> pessoa = pessoaRepository.findById(idpessoa);

		ModelAndView modelAndView = new ModelAndView("cadastro/cadastropessoa");
		modelAndView.addObject("pessoaobj", pessoa.get());

		Iterable<Pessoa> pessoasIt = pessoaRepository.findAll();
		modelAndView.addObject("pessoas", pessoasIt);
		
		modelAndView.addObject("profissao", profissaoRepository.findAll()); 
		return modelAndView;

	}


	@GetMapping("/removerpessoa/{idpessoa}")
	public ModelAndView excluir(@PathVariable("idpessoa") Long idpessoa) {
		// ModelAndView indica que estou enviando infomações para tela

		pessoaRepository.deleteById(idpessoa);

		ModelAndView modelAndView = new ModelAndView("cadastro/cadastropessoa");
		// cadastro/cadastropessoa indica que vou ficar na mesma tela
		modelAndView.addObject("pessoas", pessoaRepository.findAll());
		modelAndView.addObject("pessoaobj", new Pessoa());
		return modelAndView;

	}

	@PostMapping("**/pesquisarpessoa") // **ignora tudo antes da url
	public ModelAndView pesquisar(@RequestParam("nomepesquisa") String nomepesquisa,
			@RequestParam("pesqsexo") String pesqsexo) {
		
		List<Pessoa> pessoas = new ArrayList<>();
		
		if(pesqsexo != null && !pesqsexo.isEmpty()) {
			pessoas =pessoaRepository.findPessoaByNameSexo(nomepesquisa, pesqsexo);
			
		}
		else {
			pessoas = pessoaRepository.findPessoaByName(nomepesquisa);
		}
		
		// ModelAndView indica que estou enviando infomações para tela
		ModelAndView modelAndView = new ModelAndView("cadastro/cadastropessoa");
		// cadastro/cadastropessoa indica que vou ficar na mesma tela
		modelAndView.addObject("pessoas", pessoas);
		modelAndView.addObject("pessoaobj", new Pessoa());

		return modelAndView;

	}
	
	//na tela troquei o metodo para get para executar esse metodo
	@GetMapping ("**/pesquisarpessoa") // **ignora tudo antes da url
	public void imprimePdf(@RequestParam("nomepesquisa") String nomepesquisa,
			@RequestParam("pesqsexo") String pesqsexo,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		List<Pessoa> pessoas = new ArrayList<Pessoa>();
		if(pesqsexo != null && !pesqsexo.isEmpty() && nomepesquisa != null && nomepesquisa.isEmpty()) {
			pessoas = pessoaRepository.findPessoaByNameSexo(nomepesquisa, pesqsexo);
		}else if(nomepesquisa != null && !nomepesquisa.isEmpty()) {
			pessoas = pessoaRepository.findPessoaByName(nomepesquisa);
			
		}else {
			Iterable<Pessoa> iterator =  pessoaRepository.findAll();
			for (Pessoa pessoa : iterator) {
				pessoas.add(pessoa);
			}
		}
		
		//chama o servico de geração                   //rel tem q ser o nome da arq
		byte[] pdf  = reportUtil.geraRelatorio(pessoas, "rel", request.getServletContext());
		
		//tamanho da resposta para navegador
		response.setContentLength(pdf.length);
		
		//definir na resposta tipo de arquivo
		response.setContentType("application/octet-stream"); //esse tipod e resposta serve pra qualquer coisa application/octet-stream
		
		//difinir o cabecalho
		String headerkey = "Content-Disposition";
		String headervalue = String.format("attachment; filename=\"%s\"", "relatorio.pdf");
		response.setHeader(headerkey, headervalue);
		
		//finaliza resposta pro navegador
		response.getOutputStream().write(pdf);
		
	}

	@PostMapping("**/addfonePessoa/{pessoaid}")
	public ModelAndView addFonePessoa(Telefone telefone, @PathVariable("pessoaid") Long pessoaid) {

		Pessoa pessoa = pessoaRepository.findById(pessoaid).get();
		telefone.setPessoa(pessoa);

		telefoneRepository.save(telefone);

		ModelAndView modelAndView = new ModelAndView("cadastro/telefones");
		modelAndView.addObject("pessoaobj", pessoa);
		// essa loinha faz carregar a lista de telefones
		modelAndView.addObject("telefones", telefoneRepository.getTelefones(pessoaid));
		return modelAndView;
	}
	
	@GetMapping("/telefones/{idpessoa}")
	public ModelAndView telefones(@PathVariable("idpessoa") Long idpessoa) {
		
		Optional<Pessoa> pessoa = pessoaRepository.findById(idpessoa);

		ModelAndView modelAndView = new ModelAndView("cadastro/telefones");
		modelAndView.addObject("pessoaobj", pessoa.get());
		modelAndView.addObject("telefones", telefoneRepository.getTelefones(idpessoa));
		return modelAndView;
		
	}
	
	@GetMapping("/removertelefone/{idtelefone}")
	public ModelAndView removerTelefone(@PathVariable("idtelefone") Long idtelefone) {
		// idtelefone é o parametro que passei da tela

		Pessoa pessoa = telefoneRepository.findById(idtelefone).get().getPessoa();
		telefoneRepository.deleteById(idtelefone);

		ModelAndView modelAndView = new ModelAndView("cadastro/telefones");
		modelAndView.addObject("pessoaobj", pessoa);
		modelAndView.addObject("telefones", telefoneRepository.getTelefones(pessoa.getId()));
		//para excluir o reg pai junto com filhos é só ir no mapeamento da classe pessoa
		//e incluir uma anotação
	
		return modelAndView;

	}
	
}
