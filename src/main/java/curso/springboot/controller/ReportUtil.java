package curso.springboot.controller;

/*classe de geração de pdf
 * primeiro importar as bibliotecas do jasper
 * segundo criar essa classe que vai ser generica
 * 3º criar a classe no cotrole 
 * 4º criar o botão de pdf na tela*/

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.stereotype.Component;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Component
public class ReportUtil implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/*classe generica para gerar relatorios, vai servir para qualquer rel */
	
	//retorna nosso pdf para download no navegador
	public byte[] geraRelatorio (List listDados, String relatorio, ServletContext servletContext) throws Exception {
		
		//cria lista de dado para o relatorio com nossa lista de obj para imprimir
		JRBeanCollectionDataSource jrbcds = new JRBeanCollectionDataSource(listDados);
		
		//carrega o caminho do arquivo jasper compilado
		String caminhoJasper = servletContext.getRealPath("relatorios") + File.separator + relatorio +".jasper";
		
		//carrega o arquivo jasper passando os dados
		JasperPrint impressoraJasper = JasperFillManager.fillReport(caminhoJasper, new HashMap(), jrbcds);
		
		//exporta para byte[] para fazer o download de pdf
		return JasperExportManager.exportReportToPdf(impressoraJasper);
		
	}

}
